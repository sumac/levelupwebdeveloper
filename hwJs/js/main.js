 function swap()
 {   
    console.log(`--------------------SWAP----------------------`);

      let a = 5;
      let b = 10;
      console.log(`a = ${a} b = ${b}`);

      let temp = a;
      a = b
      b = temp;

      console.log(`Swap a  = ${a} b = ${b}`); 
 }

 function triangle(a, b)
 {
    console.log(`--------------------Triangle----------------------`);
    
    console.log(`a = ${a} b = ${b}`); 
    console.log(`Sqrt ${Math.sqrt(a*a + b*b)}`);  
 }


 function line(x1, y1, x2, y2)
 {
    console.log(`--------------------Line----------------------`);
    
    console.log(`x1 = ${x1} y1 = ${y1} x2 = ${x2} y2 = ${y2}`); 
    let k = (y1 - y2) / (x1 - x2);
    let b = y2 - k * x2;

    console.log(`y = ${k} * x  + ${b}`); 
 }

 function rnd(min, max) 
 {  
    return Math.floor(Math.random() * (max - min + 1) ) + min; 
}  
  
 function max()
 {

    console.log(`--------------------Max----------------------`);

    let a = rnd(-10,10);
    let b = rnd(-10,10);
    let c = rnd(-10,10);

    let mult, sum;

    console.log(`a = ${a} b = ${b} c = ${c}`);
    console.log(`Multireault ${mult = a*b*c}`);
    console.log(`SumResult ${sum = a+b+c}`);
    console.log(`Max  ${ mult>sum ? mult:sum  }`);
       
 }


 function divide()
 { 
    console.log(`--------------------Divide----------------------`);

    let a = rnd(-100,100);
    let b = rnd(-100,100);
   
    if(b == 0){
        console.log(`a=${a} не делится на b=${b}`);
        return;
    } 

    console.log(`a = ${a} b = ${b}`);  
    console.log(`Остаток = ${a%b}`);
    console.log(`Частное = ${a/b}`); 
 } 

 function four()
 { 
    console.log(`--------------------Four----------------------`);

    let x = rnd(-10,10);
    let y = rnd(-10,10); 

    console.log(`x = ${x} y = ${y}`);  

    if(x == 0 && y == 0)
    {
      console.log(`Это центр!`);
      return; 
    }

    if(x < 0 && y >0 )
    {
        console.log(`1 четверть`);
    }
    else if(x > 0 && y > 0)
    {
        console.log(`2 четверть`);
    }
    else if(x > 0 && y < 0)
    {
        console.log(`3 четверть`);
    }
    else  
    {
        console.log(`4 четверть`);
    }  
 }


 function round()
 { 
    console.log(`--------------------Round----------------------`);

    let x = rnd(-15,15);
    let y = rnd(-15,15);  
    let r = rnd(0, 10); 

    console.log(`x = ${x} y = ${y} radius = ${r}`);  

    let pointRadius = Math.sqrt(x*x + y*y);

    console.log(`${ pointRadius > r ? "Не принадлежит" : "Принадлежит"}`);  
    
 }

 function sqrt()
 {
   console.log(`--------------------Sqrt----------------------`);

   let a = rnd(-15,15);
   let b = rnd(-15,15);  
   let c = rnd(-15,15);  

   console.log(`a = ${a} b = ${b} c = ${c}`);  
   if(a==0)
   {
      console.log(`a Не может быть равен 0`);
      return;
   }

   let d = b*b - 4*a*c; 
   
    if(d > 0)
    {
      return  console.log(`x1 = ${  ( -1*b + Math.sqrt(d) )/ (2*a)  } x2 = ${  ( -1*b - Math.sqrt(d) )/ (2*a)  }`);  
    }
    else if(d < 0)
    {
      return  console.log(`x1,x2 = ${   -1*b/ (2*a)  }`);  
    }
    else
    {
      console.log(`Решений нет`);
    } 
 }


 function fact()
 {
   console.log(`--------------------Fact----------------------`); 

   let n = rnd(1,15);
   console.log(`n = ${n}`);  

   let nn = 1;
   for(i=1; i < n+1; ++i)
   {
      nn = nn*i;
   }

   console.log(`n! = ${nn}`);
 }



 function table()
 {
   console.log(`--------------------Table----------------------`); 

   let min = 1;
   let max = 30;
   let step = 2;

   console.log(`min = ${min} max = ${max} step = ${step}`);   

   let x = min;

   console.log(`--------------------------------------`); 
   console.log(`    Y          |               X       `); 
   console.log(`--------------------------------------`); 
   while(x <= max)
   {
      let y = -1 * 0.23 * x * x + x 
      console.log(`y = ${y}    |     x = ${x}`);   
      x += step
   } 
 }
 
 function nature()
 {
   console.log(`--------------------Nature----------------------`); 

   let digit = rnd(10000 , 100000000); 

   console.log(`digit = ${digit}`);   


   let sum = 0;
   let mult = 1;
   while(true)
    {
         let res = digit % 10;
        
         digit = (digit - res) / 10;
         sum += res;
         mult *= res;

         console.log(`res = ${res}`);  

         if(digit < 1)
         {
            break;
         } 
    }

    console.log(`sum = ${sum}  mult = ${mult}`);   
 }

 //1+2+...+n = n(n+1)/2
 function set()
 {
   console.log(`--------------------Set----------------------`); 
 
   let n = rnd(1 , 100000000); 

   n1 = n*(n+1)/2;

   let sum = 0;
   for(i=1; i< n + 1; i++)
   {
      sum+=i;
   }

   if(n1==sum)
   {
      console.log(`Равно при N=${n}`);   
   }
   else
   {
      console.log(`Не равно при N=${n}`);   
   } 
 }
 
 function fibonachi()
 {
   console.log(`--------------------Fibonachi----------------------`); 

   let n = rnd(1 , 100); 
   
   if (n <= 2)
   {
      console.log(`${1}`);   
      return;
   } 
   
   let x = 1;
   console.log(`${1}`);  
   let y = 1;
   console.log(`${1}`);  
   let ans = 0;
   for (var i = 1; i < n; i++)
   {
     ans = x + y;
     x = y;
     y = ans;
     console.log(`${ans}`);  

   }
 
}


function massive()
{
     console.log(`--------------------Massive----------------------`); 

     let n = 20;
     let m = [];

     let index = 0;
     let value = 0;
     let temp = 0;
     for(i=0; i<n; i++)
     {
         value = m[i] = rnd(-100,100); 
         console.log(`${value}`);  
         if(temp < value)
         {
            temp = value;
            index = i;
         }
     }
     console.log(`Max value = ${temp} Index =${index} `);    
}

function reverse()
{
     console.log(`--------------------Reverse----------------------`); 

     let n = 20;
     let m = []; 
  
     for(i=0; i<n; i++)
     {
         m[i] = rnd(-100,100); 
         console.log(`${ m[i]}`);   
     }
     console.log(`-------------------------------`);

     for(i=0; i<n; i++)
     {
         let a = m[i];
         let b = m[n-1-i]

         m[i] = b;
         m[n-1-i] = a; 

         if(i > (n-1-i))
         {
            break;
         } 
     }

     for(i=0; i<n; i++)
     {
       console.log(`${ m[i]}`);   
     }
    
}

function sideReverse()
{
     console.log(`--------------------Side Reverse----------------------`); 

     let m = [1,2,3,4,5,6,7,8,9]; 
     
     for(i=0; i<m.length; i++)
     {
         console.log(`${ m[i]}`);   
     }

     let d = m.length%2;
     let center = (m.length - m.length%2)/2 + d;

     for(i=0; i<m.length; i++)
     {  
         if(center + i == m.length)
         {
            break;
         } 
        let a =  m[i];
        m[i] = m[center + i];  
        m[center + i] = a; 
       
     } 
     console.log(`----result-----`); 
     for(i=0; i<m.length; i++)
     {
         console.log(`${ m[i]}`);   
     }
}

function middleSum()
{
     console.log(`--------------------MiddleSum----------------------`);
     
     let m = [1,2,3,4,5,6,7,8,9]; 
     
     let sum = 0;
     for(i=0; i<m.length; i++)
     {
         console.log(`${m[i]}`); 
         
         sum+=m[i]
     }
     
     let middle = sum/m.length;

     console.log(`Среднее ${middle}`);  

     for(i=0; i<m.length; i++)
     {
        if( m[i] > middle)
        {
            console.log(`Элемент ${ m[i] }`);  
        }
     }
}


function isInteger(n) {
   return n === +n && n === (n|0);
}

function maxSum()
{
     console.log(`--------------------MaxSum----------------------`);
     
     let n = 10;
     let m = []; 
  
     for(i=0; i<n; i++)
     {
         m[i] = rnd(0,30); 
         console.log(`${ m[i]}`);   
     }
     console.log(`-------------------------------`);


     let max = m[0];
     let min = m[0];
     let tmpIndex = [0,0];
     for(i=0; i<n; i++)
     { 
         if(m[i] > max)
         {
            max = m[i];
            tmpIndex[0] = i;
         }
         if(m[i] < min)
         {
            min = m[i];
            tmpIndex[1] = i;
         }
     }
     console.log(`Max ${max}`);
     console.log(`Min ${min}`);

     let sum = 0;
     let startIndex = tmpIndex[0];
     let endIndex = tmpIndex[1];
     if(tmpIndex[0] > tmpIndex[1])
     {
         startIndex = tmpIndex[1];
         endIndex = tmpIndex[0];
     }

     for(i = startIndex + 1; i < endIndex; i++)
     {
         sum+=m[i] 
     }
     console.log(`Sum ${sum}`);

}


 

function isEven(d)
{   
   if(d < 0)
   {
      d += 2;
   }
   else
   {
      d -= 2;
   }
   if( d == 0)
   {
      return true;
   }
   else if(d== -1)
   {
      return false;
   } 
   return isEven(d)
}

function recourse()
{ 
   console.log(`--------------------isEven----------------------`); 
   console.log(isEven(50)); 
   console.log(isEven(75)); 
   console.log(isEven(-1)); 
   console.log(isEven(-13)); 
   console.log(isEven(2)); 
   console.log(isEven(8)); 
   console.log(isEven(-27)); 
}

function countBs(str)
{ 
   console.log(`--------------------countBs----------------------`); 

      let counter = 0;
      for(i=0;i<str.length;i++)
      {
          let b = str.charAt(i);

         if( b === "B")
         {
            counter++;
         }  
      } 
      return counter;
}

function countChar(str, val)
{
   console.log(`--------------------countChar----------------------`); 

      let counter = 0;
      for(i=0;i<str.length;i++)
      {
         let b = str.charAt(i);

         if( b === val)
         {
            counter++;
         }  
      } 
      return counter;
}


function range(a,b)
{ 
   console.log(`--------------------range----------------------`); 

   let m = [];
   for(i=0; i<b-a + 1; i++)
   {
      m[i] = a + i;
      console.log(m[i]); 
   }  
   return m; 
}


function sum(m)
{ 
   console.log(`--------------------sum----------------------`); 

   let sum = 0;
   for(i=0; i<m.length; i++)
   {
      sum+=m[i]  
     
   } 
   console.log(sum);  
}

function reverse2(m)
{ 
   console.log(`--------------------reverse2----------------------`); 
 
   var ret = [];
   for(let i = m.length-1; i >= 0; i--)
   {
       ret.push(m[i]);
   }
  
   for(i=0; i<ret.length; i++ )
   {
      console.log(ret[i]);  
   }

   return ret; 
}

function reverse3(m)
{ 
   console.log(`--------------------reverse3----------------------`); 
  
   let n = m.length;
   for(i=0; i<n; i++)
   {
         let a = m[i];
         let b = m[n-1-i]

         m[i] = b;
         m[n-1-i] = a; 

         if(i >= (n-1-i))
         {
            break;
         } 
   }
  
   for(i=0; i<n; i++ )
   {
      console.log(m[i]);  
   }

   return m; 
}



function listAdd( elem, m, index, cur)
{ 
   while(true)
   { 
      let e;
      if(!cur)
      {
         cur = {value : m[index], next : null}
         elem[0] = cur;
      }
      else
      { 
         cur = cur.next = {value : m[index], next : null}
      }
      
      index++;
      if(index == m.length)
      {
         return;
      }
      return listAdd(elem, m, index, cur); 
   } 
}

function listt(m)
{ 
   console.log(`--------------------list----------------------`); 
  
   let n = m.length;
   let elem = {};
   let index  = 0;
   listAdd(elem, m, index, null);

   return elem; 
}

function listToArray()
{
   console.log(`--------------------list----------------------`); 

   let list = {
      value: 1,
      next: {
        value: 2,
        next: {
          value: 3,
          next: {
            value: 4,
            next: null
          }
        }
      }
    };

    
   let a = []
   let cur = list
   while(true)
   { 
         if(cur.next != null)
         {
            a.push(cur.value)
            cur = cur.next;
         }
         else
         {
            return a;
         }
    } 
}

function prepend(){


   console.log(`--------------------prepend----------------------`); 

   let val =  10;

   let list = {
      value: 1,
      next: {
        value: 2,
        next: {
          value: 3,
          next: {
            value: 4,
            next: null
          }
        }
      }
    };


  let newList = {value : val, next : list}; 
  return  newList; 

}

function nth(d)
{ 
   console.log(`--------------------nth----------------------`); 
   let list = {
      value: 1,
      next: {
        value: 2,
        next: {
          value: 3,
          next: {
            value: 4,
            next: null
          }
        }
      }
    };

    let elem =  getElem(d, list,  0)
    return elem;
}


function getElem(d, cur, counter)
{ 
      if(counter == d)
      {
         return cur.value;
      }
      else if(cur.next != null)
      {
         cur = cur.next;
         counter++;
         return getElem(d, cur, counter)
      } 
}




